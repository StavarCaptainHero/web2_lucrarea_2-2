<?php
session_start();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Input</title>
</head>
<body>

<form action="output.php" method="get">
    <input type="text" name="varname" value="<?php serialize($array_name); ?>" required placeholder="Insert numbers">
    <input type="submit" value="submit">
</form>

<?php
    $_SESSION['varname'] = $input_value;
?>


</body>
</html>